/**
 * TODO:
 *  - Separate all User related functions to a separate file
 *  - Separate all JWT functions to a separate file
 */

const jwt = require( 'jsonwebtoken' )
const faunadb = require( 'faunadb' )
const q = faunadb.query
const fdbClient = new faunadb.Client( { secret: process.env.FAUNADB_SECRET } )

/* const validateBody = ( body ) => {
  // Validation is a bit more complex than this
  return !['name','email','password'].every( ( field ) => body[field] )
} */

const checkUser = ( { email, password } ) => {
  return fdbClient.query( q.Identify( q.Match( q.Index( process.env.FAUNADB_USERS_INDEX ), email ), password ) )
    .then( () => fdbClient.query(
      q.Get(
        q.Match( q.Index( process.env.FAUNADB_USERS_INDEX ), email )
      )
    ).then( ( { data } ) => {
      console.log( 'DATA',data )

      return { message: 'Login successful!', data: { data } }
    } )
    )
    .catch( ( res ) => {
      throw { message: 'Login failed!', data: res }
    } )
}

const createUser = async ( { name, email, password } ) => {
  return fdbClient.query(
    q.Create(
      q.Collection( process.env.FAUNADB_USERS_COL ),
      { data: { name, email }, credentials: { password } }
    ),
  )
    .then( ( ) => {
      return { message: 'Registration successful!', data: { name } }
    } )
    .catch( ( err ) => {
      throw { message: 'Registration failed', error: err }
    } )
}

exports.handler = function( event, context, callback ) {
  const reqData = JSON.parse( event.body )

  if ( event['httpMethod'] === 'POST' ) {
    /** LOGIN */
    if( reqData.action === 'login' )
      return checkUser( reqData )
        .then( ( { data } )=> {
          console.log( 'WOOOT:', data )
          // Create JWT since sing in was successful
          const token = jwt.sign( {
            ...data,
            iat: Math.floor( Date.now() / 1000 ) - 30
          },
          process.env.JWT_SECRET
          )

          callback( null, {
            statusCode: 200,
            headers: { 'Set-Cookie': `dg_session=${token};Domain=localhost;Path=/;` },
            body: 'SUCCESS'
          } )
        } ).catch( ( err )=> {
          console.log( err )

          callback( null, {
            statusCode: 500,
            body: JSON.stringify( err )
          } )
        } )
    
    /** REGISTRATION */
    if( reqData.action === 'register' )
      return createUser( reqData )
        .then( ( { data: rawData } )=> {
          callback( null, {
            statusCode: 200,
            body: JSON.stringify( rawData )
          } )
        } ).catch( ( err )=> {
          console.log( err )
          callback( null, {
            statusCode: 500,
            body: err
          } )
        } )
  }

  return callback(
    new Error( `Unexpected HTTP method "${event['httpMethod']}"` )
  )
}