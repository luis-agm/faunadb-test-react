exports.handler = function( event, context, callback ) {
  const faunadb = require( 'faunadb' )
  const q = faunadb.query
  const fdbClient = new faunadb.Client( { secret: process.env.FAUNADB_SECRET } )

  fdbClient.query(
    q.Map( 
      q.Paginate(
        q.Documents( q.Collection( process.env.FAUNADB_USERS_COL ) )
      ),
      q.Lambda( x => q.Get( x ) ) 
    )
  ).then( ( { data: rawData } )=> {
    rawData.map( ( { data } )=> console.log( JSON.stringify( data, null, 4 ) ) )
    callback( null, {
      statusCode: 200,
      body: JSON.stringify( rawData )
    } )
  } ).catch( ( err )=> {
    console.log( err )
    callback( null, {
      statusCode: 500,
      body: err
    } )
  } )
}