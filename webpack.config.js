const HtmlWebPackPlugin = require( 'html-webpack-plugin' )
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' )
const Dotenv = require( 'dotenv-webpack' )
const path = require( 'path' )

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader']
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      components: path.resolve( __dirname, 'src/components/' )
    }
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/'
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new HtmlWebPackPlugin( {
      template: './src/index.html'
    } ),
    new Dotenv()
  ],
  devServer: {
    port: 8888,
    after: () => {
    },
    open: false,
    historyApiFallback: true,
  }
}
