import { useState } from 'react'

const useCredentials = () => {
  const [creds, setCreds] = useState( { name: '', email: '', password: '' } )

  const setCred = ( evt ) => {
    const credName = evt.target.id
    const credVal = evt.target.value

    setCreds( {
      ...creds,
      [credName]: credVal
    } )
  }

  return [creds, setCred]
}

export default useCredentials