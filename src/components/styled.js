import styled from 'styled-components'

export const Button = styled.button`
    border-radius: 4px;
    color: white;
    background-color: skyblue;
    outline: none;
    border: none;
    height: 40px;
    

    &:active {
        border: none;
        background-color: slateblue;
    }  

    &:focus {
        outline: none;
    }
`

export const NavBar = styled.nav`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    padding: 0 3rem;
    background-color: darkcyan;
    height: 70px;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    color: white;

    a {
        text-decoration: none;
        color: white;
        font-size: 18px;
    }
`

export const AppContainer = styled.div`
    padding-top: 70px;
`

export const PageContainer = styled.div`
    display: flex;
`

export const List = styled.ul`
    padding: 10px;
    background-color: skyblue;
    border-radius: 5px;
`

export const Form = styled.form`
    margin: 15px 0;
`

export const FormGroup = styled.div`
    & > label {
        width: 100%;
        margin-bottom: 5px;
    }
`

export const Label = styled.label``

export const Input = styled.input`
    height: 2.5rem;
    padding: 0 0.5rem;
    outline: none;
    background: white;
    border: 1px solid gray;
    border-radius: 5px;

    &:focus {
        box-shadow: rgba(20, 20, 20, 0.27) 0px 0px 5px;
    }
`