import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import ck from 'js-cookie'

const PrivateRoute = ( { component: Component, ...rest } ) => {
  const validate = () => {
    const jwt = ck.get( 'dg_session' )
    let session
    try {
      if ( jwt ) {
        const base64Url = jwt.split( '.' )[1]
        const base64 = base64Url.replace( '-', '+' ).replace( '_', '/' )
        session = JSON.parse( window.atob( base64 ) )
      }
    } catch ( error ) {
      console.log( error )
    }
    return session 
  }
    
  const renderComponent = ( props ) => validate() ? 
    <Component {...props} /> : 
    <Redirect to={{ pathname: '/login', state: { from: props.location } }} />

  return (
    <Route {...rest} render={renderComponent} />
  )
}

export default PrivateRoute
