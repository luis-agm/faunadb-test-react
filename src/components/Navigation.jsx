import React from 'react'
import { NavBar } from './styled'
import { Link } from 'react-router-dom'


const Navigation = () => {
  return (
    <NavBar>
      <Link to='/'>Sign In</Link>
      <Link to='/register'>Register</Link>
    </NavBar>
  )
}

export default Navigation
