import React from 'react'
import { List } from './styled'

export default ( { users } ) => {
  return users.length ? (
    <List>
      {
        users.map( ( { data: user } ) => <li key={user.name}>{user.name}</li> ) 
      }
    </List>
  ) : null
}
