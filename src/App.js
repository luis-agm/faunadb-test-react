import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import { AppContainer } from './components/styled'
import Login from './pages/auth/Login'
import Register from './pages/auth/Register'
import Home from './pages/auth/Home'
import Navigation from './components/Navigation'
import PrivateRoute from './components/PrivateRoute'

const App = () => {
  return (
    <Router>
      <div className='App'>
        <Navigation />
        <AppContainer>
          <Switch>
            <PrivateRoute exact path='/' component={Home} />
            <Route path='/login' component={Login} />
            <Route path='/register' component={Register} />
          </Switch>
        </AppContainer>
      </div>
    </Router> 
  )
}

export default App
