import React from 'react'
import { PageContainer } from '../../components/styled'

const Home = () => {
  return (
    <PageContainer>
      Hello World!
    </PageContainer>
  )
}

export default Home
