import React from 'react'
import axios from 'axios'
import { Button, Input, Form, FormGroup, Label } from '../../components/styled'
import useCredentials from '../../hooks/useCredentials'

const Register = () => {
  const [creds, setCred] = useCredentials()

  const clickHandler = () => {
    axios.post( '/.netlify/functions/auth', { ...creds, action:'register' } )
      .then( ( ) => alert( 'Registered successfully!' ) )
  }

  return (
    <div className='login-page'>
      <h1>Sign up</h1>
      <Form>
        <FormGroup>
          <Label>Name:</Label>
          <Input type='text' id='name' onChange={setCred} />
        </FormGroup>
        <FormGroup>
          <Label>Email:</Label>
          <Input type='email' id='email' onChange={setCred} />
        </FormGroup>
        <FormGroup>
          <Label>Password:</Label>
          <Input type='password' id='password' onChange={setCred} />
        </FormGroup>
      </Form>
      <Button onClick={clickHandler}>Sign up!</Button>
    </div>
  )
}

export default Register