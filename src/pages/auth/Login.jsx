import React from 'react'
import axios from 'axios'
import { Button, Input, Form, FormGroup, Label } from '../../components/styled'
import useCredentials from '../../hooks/useCredentials'

const Login = () => {
  const [creds, setCred] = useCredentials()

  const clickHandler = () => {
    axios.post( '/.netlify/functions/auth', { ...creds, action:'login' }, { credentials: 'same-origin' } )
      .then( ( response ) => alert( 'Signed in successfully!', JSON.stringify( response ) ) )
  }

  return (
    <div className='login-page'>
      <h1>Sign in</h1>
      <Form>
        <FormGroup>
          <Label>Email:</Label>
          <Input type='email' id='email' onChange={setCred} />
        </FormGroup>
        <FormGroup>
          <Label>Password:</Label>
          <Input type='password' id='password' onChange={setCred} />
        </FormGroup>
      </Form>
      <Button onClick={clickHandler}>Hello</Button>
    </div>
  )
}

export default Login