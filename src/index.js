import App from './App'
import React from 'react'
import ReactDOM from 'react-dom'

global.isClient = typeof window !== undefined

const wrapper = document.getElementById( 'create-greeting-form' )
ReactDOM.render( <App />, wrapper )
